package model;

class Mine extends Box{
    private boolean activated;
    public Mine(int i, int j){
        super(i, j);
        this.activated=false;
    }
    public boolean activated(){
        activated = true;
        return this.activated;
    }
    public boolean getActivated(){
        return this.activated;
    }
}