package model;

class createBoard{
    public static Box[][] getBoard(int size){
        Box[][] board = new Box[size][size];
        for(int i=0; i<size; i++){
            for(int j=0; j<size; j++){
                board [i][j] = new Box(i,j);
            }
        }
        return board;
    }
}